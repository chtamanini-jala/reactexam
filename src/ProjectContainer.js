import React, { Component } from "react";
import NavBarContainer from "./NavBarContainer";
import BoardContainer from "./BoardContainer";
import CalendarContainer from "./CalendarContainer";

export default class ProjectContainer extends Component{
    constructor(props) {
        super(props);
    }    

    render(){
        return(
            <div className="project-container">
                <div className="navbar-container">
                    <NavBarContainer/>
                </div>
                <div className="board-container">
                    <BoardContainer projects={this.props.projects}/>
                </div>
                <div className="calendar-container">
                    <CalendarContainer />
                </div>
            </div>
        );
    }
}