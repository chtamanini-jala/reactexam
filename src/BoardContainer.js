import React, { Component } from "react";
import BoardInfoContainer from "./BoardInfoContainer";
import BoardProjectsContainer from "./BoardProjectsContainer";

function BoardContainer({projects}){
    return(
        <div>
            <div className="board-info-container">
                <BoardInfoContainer/>
            </div>
            <div className="board-projects-container">
                <BoardProjectsContainer projects={projects}/>
            </div>
        </div>
    );
}

export default BoardContainer;