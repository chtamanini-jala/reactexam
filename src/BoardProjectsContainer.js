import React from "react";
import PropTypes from "prop-types";
import BoardProjectsItem from "./BoardProjectsItem";

function BoardProjectsContainer({projects}){
    const projectsData = [];

    projects.forEach(project => {
        projectsData.push(
            <BoardProjectsItem
                name={project.name}
                qty={project.quantity}
            />
        );
    });

    return(
        <div>
            <h3>All</h3>
            <div>
                <div className="board-project-grid">
                    <div className="board-project-grid-additem">
                        <button>+</button>
                        <h5>New Project</h5>
                    </div>
                    {projectsData}
                </div>
            </div>
        </div>
    );
}

BoardProjectsContainer.propTypes ={
    projects: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            quantity: PropTypes.number,
        })
    ),
};
BoardProjectsContainer.defaultProps ={
    projects: [],
};

export default BoardProjectsContainer;