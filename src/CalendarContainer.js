import React, { Component } from "react";
import CalendarScheduleContainer from "./CalendarScheduleContainer";
import CalendarTasksContainer from "./CalendarTasksContainer";

function CalendarContainer(){
    return(
        <div>
            <div className="calendar-items-container">
                <CalendarTasksContainer/>
            </div>
            <div className="calendar-items-container">
                <CalendarScheduleContainer/>
            </div>
        </div>
    );
}

export default CalendarContainer;