import React from "react";

function NavBarContainer(){
    return(
        <div>
            <div>
                <div className="navbar-item">
                    <a>home</a>
                </div>
                <div className="navbar-item">
                    <button>srch</button>
                </div>
            </div>
            <div className="navbar-item-space"></div>
            <div>
                <div className="navbar-item">
                    <a>bell</a>
                </div>
                <div className="navbar-item">
                    <a>!</a>
                </div>
                <div className="navbar-item">
                    <a>help</a>
                </div>
                <div className="navbar-item">
                    <a>profile</a>
                </div>
            </div>
        </div>
    );
}

export default NavBarContainer;